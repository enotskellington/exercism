// Leap stub file

// The package name is expected by the test program.
package leap

// testVersion should match the targetTestVersion in the test file.
const testVersion = 2

// It's good style to write a comment here documenting IsLeapYear.
func IsLeapYear(y int) bool {
	// a leap year is ..
	if y%4 == 0 { // every year divisible by 4
		if y%100 == 0 {
			if y%400 == 0 {
				return true
			}
			return false
		}
		return true
	}
	return false
}
