package greeting

const testVersion = 3

func HelloWorld(in string) string {
	out := "Hello, "
	if len(in) > 1 {
		out += in
	} else {
		out += "World"
	}
	return out + "!"
}
